using UnityEngine;

public struct CameraDistanceComponent
{
    public Vector3 Distance;
}
