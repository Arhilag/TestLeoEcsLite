﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

sealed class LookAtSystem : IEcsRunSystem
{
    readonly EcsFilterInject<Inc<DirectionComponent,
        ModelComponent>, Exc<ProjectileTag>> _filter = default;
    readonly EcsPoolInject<DirectionComponent> _directionPool = default;
    readonly EcsPoolInject<ModelComponent> _modelPool = default;
    
    public void Run(EcsSystems systems)
    {
        var filter = _filter.Value;
        var directionPool = _directionPool.Value;
        var modelPool = _modelPool.Value;
        
        foreach (var i in filter)
        {
            ref var directionComponent = ref directionPool.Get(i);
            ref var modelComponent = ref modelPool.Get(i);

            modelComponent.modelTransform.LookAt(modelComponent.modelTransform.position + directionComponent.Direction);
        }
    }
}