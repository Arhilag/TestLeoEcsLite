﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

sealed class AnimationSystem : IEcsRunSystem
{
    readonly EcsFilterInject<Inc<AnimatorComponent,
        DirectionComponent>> _filter = default;
    readonly EcsPoolInject<AnimatorComponent> _animatorPool = default;
    readonly EcsPoolInject<DirectionComponent> _directionPool = default;

    public void Run(EcsSystems systems)
    {
        var filter = _filter.Value;
        var animator = _animatorPool.Value;
        var direction = _directionPool.Value;
        
        foreach (var entity in filter)
        {
            ref var animatorComponent = ref animator.Get(entity);
            ref var directionComponent = ref direction.Get(entity);
            if (animatorComponent.Animator)
            {
                animatorComponent.Animator.SetFloat("Speed", directionComponent.Direction.magnitude, 0.1f, Time.deltaTime);
            }
        }
    }
}