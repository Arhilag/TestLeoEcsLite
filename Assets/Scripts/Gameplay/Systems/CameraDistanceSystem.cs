﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

sealed class CameraDistanceSystem : IEcsRunSystem, IEcsInitSystem
{
    readonly EcsFilterInject<Inc<ModelComponent,
        PlayerTag>> _filterPlayer = default;
    readonly EcsPoolInject<ModelComponent> _modelPool = default;
    
    readonly EcsFilterInject<Inc<ModelComponent,
        CameraDistanceComponent>> _filterCamera = default;
    readonly EcsPoolInject<CameraDistanceComponent> _cameraPool = default;
    
    public void Init(EcsSystems systems)
    {
        var filterPlayer = _filterPlayer.Value;
        var filterCamera = _filterCamera.Value;
        var modelPool = _modelPool.Value;
        var cameraPool = _cameraPool.Value;
        
        foreach (var entity in filterPlayer)
        {
            ref var modelPlayer = ref modelPool.Get(entity);
            foreach (var i in filterCamera)
            {
                ref var cameraDistance = ref cameraPool.Get(i);
                ref var modelCamera = ref modelPool.Get(i);
                cameraDistance.Distance = modelCamera.modelTransform.position - modelPlayer.modelTransform.position;
            }
        }
    }
    
    public void Run(EcsSystems systems)
    {
        var filterPlayer = _filterPlayer.Value;
        var filterCamera = _filterCamera.Value;
        var modelPool = _modelPool.Value;
        var cameraPool = _cameraPool.Value;
        
        foreach (var entity in filterPlayer)
        {
            ref var modelPlayer = ref modelPool.Get(entity);
            foreach (var i in filterCamera)
            {
                ref var cameraDistance = ref cameraPool.Get(i);
                ref var modelCamera = ref modelPool.Get(i);
                modelCamera.modelTransform.position = modelPlayer.modelTransform.position + cameraDistance.Distance;
            }
        }
    }
}